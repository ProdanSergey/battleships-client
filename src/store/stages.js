const COMPLETE_STAGE = 'COMPLETE_STAGE';
const RESET_STAGE = 'RESET_STAGE';

const state = {
  recruiting: false,
  briffing: false,
  intelligence: false,
  draft: false,
  brawl: false
};

const actions = {
  setStageAsCompleted({ commit }, stageName) {
    commit(COMPLETE_STAGE, stageName);
  },
  setStageAsIncompleted({ commit }, stageName) {
    commit(RESET_STAGE, stageName);
  }
};

const mutations = {
  [COMPLETE_STAGE](state, payload) {
    state[payload] = true;
  },
  [RESET_STAGE](state, payload) {
    state[payload] = false;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
