import Vue from 'vue';
import Vuex from 'vuex';

import stages from './stages';
import user from './user';
import operation from './operation';
import brawl from './brawl';

Vue.use(Vuex);

const RESET_STORE = 'RESET_STORE';

const modules = {
  stages,
  user,
  operation,
  brawl
};

const initials = JSON.stringify(modules);

const actions = {
  resetStore({ commit }) {
    commit(RESET_STORE, ['user']);
  }
};

const mutations = {
  [RESET_STORE](state, payload) {
    const prevState = JSON.parse(initials);

    Object.keys(prevState).forEach(module => {
      if (!payload.includes(module)) {
        state[module] = prevState[module].state;
      }
    });
  }
};

export default new Vuex.Store({
  actions,
  mutations,
  modules
});
