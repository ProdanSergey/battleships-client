import { OPERATION_STAGES_MAP } from '../constants';

const SET_OPERATION_PARAM = 'SET_OPERATION_PARAM';
const SOCKET_OPERATION_COMMITED = 'SOCKET_OPERATION_COMMITED';
const SOCKET_OPERATION_LOCKED_BY = 'SOCKET_OPERATION_LOCKED_BY';

const state = {
  loading: true,
  opponentId: void 0,
  params: {
    name: '',
    stage: OPERATION_STAGES_MAP.NEW,
    battlefieldSize: 10
  }
};

const actions = {
  setOperationParam({ commit }, payload) {
    commit(SET_OPERATION_PARAM, payload);
  }
};

const mutations = {
  [SET_OPERATION_PARAM](state, { param, value }) {
    state.params[param] = value;
  },
  [SOCKET_OPERATION_COMMITED](state, payload) {
    state.loading = false;
    state.params = payload;
  },
  [SOCKET_OPERATION_LOCKED_BY](state, payload) {
    state.opponentId = payload;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
