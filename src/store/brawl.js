const SOCKET_BRAWL_COMMITED = 'SOCKET_BRAWL_COMMITED';
const SOCKET_BRAWL_PLAYER_READY = 'SOCKET_BRAWL_PLAYER_READY';

const state = {
  loading: true,
  statuses: {},
  params: {
    ships: [],
    shoots: [],
    damage: []
  }
};

const mutations = {
  [SOCKET_BRAWL_COMMITED](state, payload) {
    state.loading = false;
    state.params = payload;
  },
  [SOCKET_BRAWL_PLAYER_READY](state, payload) {
    state.statuses = { ...state.statuses, [payload]: true };
  }
};

export default {
  namespaced: true,
  state,
  mutations
};
