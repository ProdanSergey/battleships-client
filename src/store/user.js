const SET_USER_NICKNAME = 'SET_USER_NICKNAME';
const SOCKET_USER_CONNECTED = 'SOCKET_USER_CONNECTED';

const state = {
  data: {
    nickName: ''
  }
};

const actions = {
  setUserNickname({ commit }, nickName) {
    commit(SET_USER_NICKNAME, nickName);
  }
};

const mutations = {
  [SET_USER_NICKNAME](state, payload) {
    state.data.nickName = payload;
  },
  [SOCKET_USER_CONNECTED](state, payload) {
    state.data = payload;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
