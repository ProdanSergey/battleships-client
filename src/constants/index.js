export const ALPHABET = [
  'a',
  'b',
  'c',
  'd',
  'e',
  'f',
  'g',
  'h',
  'i',
  'j',
  'k',
  'l',
  'm',
  'n',
  'o',
  'p',
  'q',
  'r',
  's',
  't',
  'u',
  'v',
  'w',
  'x',
  'y',
  'z'
];

export const GAME_STAGES_MAP = {
  RECRUITING: 'recruiting',
  MODE: 'mode',
  BRIFFING: 'briffing',
  INTELLIGENCE: 'intelligence',
  DRAFT: 'draft',
  BRAWL: 'brawl'
};

export const OPERATION_STAGES_MAP = {
  NEW: 'new',
  ACCEPTED: 'accepted',
  LOCKED: 'locked',
  ENGAGED: 'engaged',
  RUNNING: 'running',
  FINISHED: 'finished'
};

export const BRAWL_STAGES_MAP = {
  NEW: 'new',
  READY: 'ready',
  FINISHED: 'finished'
};
