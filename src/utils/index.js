import range from 'lodash/range';
import take from 'lodash/take';
import { ALPHABET } from '../constants';

const getLongitude = (index, size) => {
  return ALPHABET[Math.trunc(index / size)];
};
const getLatitude = (index, size) => {
  return index > 0 ? (index % size) + 1 : 1;
};

export const getLatitudeRuller = (size = 10) => range(1, size + 1);
export const getLongitudeRuller = (size = 10) => take(ALPHABET, size);

export const buildBattleField = (size = 10) => {
  return range(size * size).reduce((map, index) => {
    const long = getLongitude(index, size);
    const lat = getLatitude(index, size);

    const key = `${long}${lat}`;

    return { ...map, [key]: { long, lat, key, ship: false, shot: false } };
  }, {});
};

export const getShipFromSelection = selection => {
  return selection.reduce(
    (ship, { dataset: { long, lat } }) => ({
      ...ship,
      gps: [...ship.gps, `${long}${lat}`],
      decks: { ...ship.decks, [`${long}${lat}`]: false }
    }),
    { gps: [], decks: {}, size: selection.length, destroyed: false }
  );
};

const LONG_MIN_LIMIT = 97;
const LONG_MAX_LIMIT = 106;

const LAT_MIN_LIMIT = 1;
const LAT_MAX_LIMIT = 10;

export const getSelectionNeighbours = (selection = []) => {
  const neighbours = [];

  if (selection.length) {
    let [firstY, ...firstX] = selection[0];
    let [lastY, ...lastX] = selection[selection.length - 1];

    firstY = firstY.charCodeAt();
    lastY = lastY.charCodeAt();

    firstX = Number(firstX.join(''));
    lastX = Number(lastX.join(''));

    const minY = firstY > LONG_MIN_LIMIT ? firstY - 1 : firstY;
    const maxY = lastY < LONG_MAX_LIMIT ? lastY + 1 : lastY;

    const minX = firstX > LAT_MIN_LIMIT ? firstX - 1 : firstX;
    const maxX = lastX < LAT_MAX_LIMIT ? lastX + 1 : lastX;

    for (let i = minY; i <= maxY; i++) {
      for (let j = minX; j <= maxX; j++) {
        const cell = String.fromCharCode(i) + j;

        !selection.includes(cell) && neighbours.push(cell);
      }
    }
  }

  return neighbours;
};

export const checkSelectionDirection = (selection = []) => {
  if (selection.length) {
    const [long, lat] = [...selection[0]];

    return selection.every(point => point.startsWith(long) || point.endsWith(lat));
  }

  return false;
};

export const getSelectionSetter = () => {
  let cache = null;

  getSelectionSetter.clearCache = function() {
    cache = null;
  };

  return function(selection) {
    if (selection.length !== cache) {
      this.selection = selection.reduce(
        (keys, { dataset: { long, lat } }) => ({
          ...keys,
          [long + lat]: true
        }),
        {}
      );

      cache = selection.length;
    }
  };
};
