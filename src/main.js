import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import io from 'socket.io-client';

import VueSocketIO from 'vue-socket.io-extended';
import Vuelidate from 'vuelidate';

import 'normalize.css';
import './styles/index.scss';

const socket = io('http://localhost:3030/');

Vue.config.productionTip = false;

Vue.use(VueSocketIO, socket, { store });
Vue.use(Vuelidate);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
