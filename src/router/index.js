import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

import store from '../store';

import { GAME_STAGES_MAP } from '../constants';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    meta: {
      stage: GAME_STAGES_MAP.RECRUITING
    },
    component: Home
  },
  {
    path: '/mode',
    name: 'mode',
    meta: {
      stage: GAME_STAGES_MAP.MODE,
      guards: [GAME_STAGES_MAP.RECRUITING]
    },
    component: () => import(/* webpackChunkName: "mode" */ '../views/Mode.vue')
  },
  {
    path: '/briffing',
    name: 'briffing',
    meta: {
      stage: GAME_STAGES_MAP.BRIFFING,
      guards: [GAME_STAGES_MAP.MODE]
    },
    component: () => import(/* webpackChunkName: "briffing" */ '../views/Briffing.vue')
  },
  {
    path: '/recon',
    name: 'recon',
    meta: {
      stage: GAME_STAGES_MAP.INTELLIGENCE,
      guards: [GAME_STAGES_MAP.RECRUITING, GAME_STAGES_MAP.BRIFFING]
    },
    component: () => import(/* webpackChunkName: "recon" */ '../views/Recon.vue')
  },
  {
    path: '/lobby',
    name: 'lobby',
    meta: {
      stage: GAME_STAGES_MAP.INTELLIGENCE,
      guards: [GAME_STAGES_MAP.RECRUITING]
    },
    component: () => import(/* webpackChunkName: "lobby" */ '../views/Lobby.vue')
  },
  {
    path: '/draft',
    name: 'draft',
    meta: {
      stage: GAME_STAGES_MAP.DRAFT,
      guards: [GAME_STAGES_MAP.INTELLIGENCE]
    },
    component: () => import(/* webpackChunkName: "draft" */ '../views/Draft.vue')
  },
  {
    path: '/brawl',
    name: 'brawl',
    meta: {
      stage: GAME_STAGES_MAP.BRAWL,
      guards: [GAME_STAGES_MAP.DRAFT]
    },
    component: () => import(/* webpackChunkName: "brawl" */ '../views/Brawl.vue')
  },
  {
    path: '/summary',
    name: 'summary',
    meta: {
      guards: [GAME_STAGES_MAP.BRAWL]
    },
    component: () => import(/* webpackChunkName: "summary" */ '../views/Summary.vue')
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.params.completed) {
    store.dispatch('stages/setStageAsCompleted', from.meta.stage);
  }

  if (to.params.restart) {
    store.dispatch('resetStore');
    next();
  }

  if (!to.meta.guards) {
    next();
  } else if (to.meta.guards.every(stage => store.state.stages[stage])) {
    next();
  } else {
    next(from.path);
  }
});

router.afterEach((to, from) => {
  if (to.params.restart) {
    return;
  }

  if (from.params.completed && !store.state.stages[from.meta.stage]) {
    store.dispatch('stages/setStageAsIncompleted', to.meta.stage);
  }
});

export default router;
