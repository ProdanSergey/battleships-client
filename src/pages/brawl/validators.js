import { getSelectionNeighbours, checkSelectionDirection } from '../../utils';

const SHIPS_LIMITS = {
  '1': 4,
  '2': 3,
  '3': 2,
  '4': 1
};

export const direction = value => {
  const selection = Object.keys(value);

  return checkSelectionDirection(selection);
};

export const radius = battlefield => value => {
  const selection = Object.keys(value);

  const neighbours = getSelectionNeighbours(selection);

  return !neighbours.some(cell => battlefield[cell].ship);
};

export const size = statuses => value => {
  const selection = Object.keys(value);

  return statuses[selection.length] !== SHIPS_LIMITS[selection.length];
};
