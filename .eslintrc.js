module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/essential", "@vue/prettier"],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-unused-vars": ["warn"],
    "prettier/prettier": [
      "warn",
      {
        "printWidth": 120,
        "semi": true,
        "singleQuote": true,
        "tabWidth": 2,
        "useTabs": false,
      }
    ]
  },
  parserOptions: {
    parser: "babel-eslint"
  }
};